"""
Homework Assignment 6: Advanced Loops
Extra Credit:

Try to determine the maximum width and height that your terminal and screen can comfortably fit without wrapping.
If someone passes a value greater than either maximum, your function should return False.
"""

def draw_playingboard_extraCredit(columns, rows):
    print('')
    if columns == 1:
        print('Columns must be greater than 1')
        return False
    elif (columns > 20) or (rows > 20):
        print('Number of columns or rows is too large to fit on screen.')
        print('Please do not execeed 20 rows or columns.')
        return False
    else:
       for r in range(rows*2):  # Needs to be doubled because need to print to lines for each row
           if r%2 == 0:  # Determine odd and even rows
               # even lines
               for c in range(columns):
                   if c < columns-1:             # Check if it is the last column
                       print('  |  ', end = '')  # Print and stay on same line
                   else:
                       print(' ')              # Print and move to next line
           else:
               # Odd lines
               if r != 2*rows-1:  # Don't print the last line
                print('-----' * (columns-1))
       return True

columns = 0
rows = 0
columns = input('Number of columns: ')
rows =    input('Number of rows:    ')
print(draw_playingboard_extraCredit(int(columns), int(rows)) )

