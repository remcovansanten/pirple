"""
Homework Assignment #3: "If" Statements
author: Remco van Santen

Details:
Create a function that accepts 3 parameters and checks for equality between any two of them.
Your function should return True if 2 or more of the parameters are equal,
and false is none of them are equal to any of the others.

Extra Credit:
Modify your function so that strings can be compared to integers if they are equivalent.
For example, if the following values are passed to your function:
6,5,"5"
You should modify it so that it returns true instead of false.
Hint: there's a built in Python function called "int" that will help you convert strings to Integers.
"""


def checkstringequal(a, b, c):
    if a == b or a == c or b == c:
        return True
    else:
        return False


print(checkstringequal('a', 'b', 'c'))
print(checkstringequal('a', 'a', 'c'))
print(checkstringequal('a', 'b', 'a'))
print(checkstringequal('a', 'b', 'b'))


# Extra Credit:
print('------------')
print('Extra Credit')
print('------------')


def checkmixedequal(a, b, c):
    if int(a) == int(b):
        return True
    elif int(a) == int(c):
        return True
    elif int(b) == int(c):
        return True
    else:
        return False


print(checkmixedequal('1', 2, '3'))
print(checkmixedequal('1', 1, 3))
print(checkmixedequal(1, '2', '1'))
print(checkmixedequal('1', '3', '3'))
