"""
Homework Assignment #8: Input and Output (I/O)

Details:
Create a note-taking program. When a user starts it up, it should prompt them for a filename.
If they enter a file name that doesn't exist, it should prompt them to enter the text they want to write to the file.
After they enter the text, it should save the file and exit.
If they enter a file name that already exists, it should ask the user if they want:
A) Read the file
B) Delete the file and start over
C) Append the file
If the user wants to read the file it should simply show the contents of the file on the screen.
If the user wants to start over then the file should be deleted and another empty one made in its place.
If a user elects to append the file, then they should be able to enter more text,
                                        and that text should be added to the existing text in the file.


Extra Credit:

Allow the user to select a 4th option:
D) Replace a single line
If the user wants to replace a single line in the file, they will then need to be prompted for 2 bits of information:
1) The line number they want to update.
2) The text that should replace that line.
"""
import os.path


def open_file(filename, mode):
    """
     Opens the file in the correct mode
    :param filename: File Name to open the file
    :param mode: mode to open the file
    :return: none
    """
    if mode == 'w' or mode == 'a':
        notesfile = open(filename, mode)
        input_line = input('Enter the text to add: ')
        notesfile.writelines(input_line+'\n')
        notesfile.close()
    elif mode == 'r':
        notesfile = open(filename, mode)
        print_lines(notesfile)


def print_lines(notesfile):
    """
    Print the lines in the file.
    Made flexible for future use
    :param notesfile: filename to open
    :return: none
    """
    for line in notesfile:
        print(line)
    notesfile.close()


def replace_line(fn):
    """
    Replaces a line in a testfile by reading all ines into a list
    Next replaces a specific list value with the new text
    and clears (trucate) the file andwrite the new values from list to the file
    :param fn: filename
    :return: none
    """
    line_nbr = int(input('Which line do you want to update?: '))
    input_line = input('Enter the new text to replace: ')
    notesfile = open(fn, 'r+')  # open file in read/write mode (r+)
    notes = notesfile.readlines()  # read all lines into list notes
    notes[line_nbr - 1] = input_line + '\n'  # replace the n-th element in list with new value
    notesfile.seek(0)  # set position to begin of file
    notesfile.truncate()  # clear contents of file
    for i in notes:  # write contents of list to file
        notesfile.writelines(i)
    notesfile.close()


# Main loop
print("Taking Notes")
filename = input('What is the filename you want to open?')
# filename = 'notes.txt' # Used for debugging and testing
if os.path.isfile(filename):  # File already exists
    print('File exists, what do you want to do?')
    print('A -> Read the file')
    print('B -> Delete the file and start over')
    print('C -> Append the file')
    print('D -> Replace a single line')
    open_choice = input('Enter your choice: ')
    if open_choice == 'A':
        open_file(filename, 'r')
    elif open_choice == 'B':
        open_file(filename, 'w')
    elif open_choice == 'C':
        open_file(filename, 'a')
    elif open_choice == 'D':
        replace_line(filename)
    else:
        print('Invalid choice')
else:  # New File
    open_file(filename, 'w')
