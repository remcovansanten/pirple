"""
Project #1: A Simple Game

Details:
Have you ever played "Connect 4"? It's a popular kid's game by the Hasbro company.
In this project, your task is create a Connect 4 game in Python.
Before you get started, please watch this video on the rules of Connect 4:
https://youtu.be/utXzIFEVPjA

Once you've got the rules down, your assignment should be fairly straightforward.
You'll want to draw the board, and allow two players to take turns placing their pieces on the board
(but as you learned above, they can only do so by choosing a column, not a row).
The first player to get 4 across or diagonal should win!
Normally the pieces would be red and black, but you can use X and O instead.

Extra Credit:
Want to try colorful pieces instead of X and O?
First you'll need to figure out how to import a package like termcolor into your project.
We're going to cover importing later in the course, but try and see if you can figure it out on your own.
Or you might be able to find unicode characters to use instead, depending on what your system supports.
Here's a hint: print(u'\u2B24')
"""
from termcolor import colored, cprint


def draw_board(field):
    """
    draws a Connect4 board, which is 7 wide (columns), 6 high (row)
    Uses currentField as input
    :return: Nothing
    """
    print('')
    for row in range(12):  # 0,1,2,3,4,5,6,7,8,9,10,11
        # 0,.,1,.,2, ........
        # if row is even row write " | |  "
        if row % 2 == 0:
            practical_row = int(row / 2)
            print("| ", end="")  # Start row with a |
            # print writing lines
            for column in range(13):  # will take values 0 (in drawing) -> 0 (in actual field), 1->., 2->1, 3->., 4->2
                # if column is even, we will print a space
                # The even columns gives us the move of each player
                if column % 2 == 0:  # Values 0,2,4, etc.
                    # The actual column that should be used in our field
                    # Make sure our values are integers
                    practical_column = int(column / 2)  # Values 0,1,2, etc
                    if practical_column != 6:
                        # Print the specific field
                        print((field[practical_column][practical_row]), end="")  # Continue in the same line
                    else:
                        print(field[practical_column][practical_row] + ' |')  # Jump to the next line
                else:
                    # The odd value just give us vertical lines
                    print(" | ", end="")
        else:
            # Odd lines
            print('------------------------------------')
    return True


def determine_empty_row(column):
    """
    Determines the first available row which is still free in the column
    :param column: column to check the free row in
    :return: the free row as integer >= 0 or -1 if column is full
    (tried if column: but this overwrites the top column)
    """
    for i in range(5, -1, -1):
        if currentField[column][i] == "  ":
            print("row " + str(i) + '= ' + currentField[column][i])
            return i
    return -1  # column is full


def determine_win_state(win_field):
    """
    Determine if a win state has occurred after the last move of a player.
    Four different loops to check horizontal, vertical, diagonal "\" and diagonal / wins
    :param win_field: the field status that has to be checked
    :return: winstate -> True if a win is detected, false if no win yet
    """
    # determine horizontal wins
    for row in range(6):
        for column in range(4):
            if win_field[column][row] != "  " and win_field[column][row] == win_field[column + 1][row] == \
                    win_field[column + 2][row] == win_field[column + 3][row]:
                print("a win on horizontal line starting in column" + str(column + 1), "and row", str(row + 1))
                return True
    # determine vertical wins
    for column in range(7):
        for row in range(3):
            if win_field[column][row] != "  " and win_field[column][row] == win_field[column][row + 1] == \
                    win_field[column][row + 2] == win_field[column][row + 3]:
                print("a win on vertical line starting in column" + str(column + 1), "and row", str(row + 1))
                return True
    # determine diagonal wins /
    for column in range(3, 7):
        for row in range(0, 3):
            if win_field[column][row] != "  " and win_field[column][row] == win_field[column - 1][row + 1] == \
                    win_field[column - 2][row + 2] == \
                    win_field[column - 3][row + 3]:
                print("a win on diagonal / line starting in column" + str(column + 1), "and row", str(row + 1))
                return True
    # determine diagonal wins \
    for column in range(4):
        for row in range(0, 3):
            if win_field[column][row] != "  " and win_field[column][row] == win_field[column + 1][row + 1] == \
                    win_field[column + 2][row + 2] == \
                    win_field[column + 3][row + 3]:
                print("a win on diagonal \\ line starting in column" + str(column + 1), "and row", str(row + 1))
                return True
    return False


if __name__ == "__main__":
    win = False
    Player = 1
    red_stone = colored(u'\u2B24', 'red')
    green_stone = colored(u'\u2B24', 'green')
    currentField = [["  ", "  ", "  ", "  ", "  ", "  "], ["  ", "  ", "  ", "  ", "  ", "  "],
                    ["  ", "  ", "  ", "  ", "  ", "  "], ["  ", "  ", "  ", "  ", "  ", "  "],
                    ["  ", "  ", "  ", "  ", "  ", "  "], ["  ", "  ", "  ", "  ", "  ", "  "],
                    ["  ", "  ", "  ", "  ", "  ", "  "]]  # A list that contains 7 lists
    draw_board(currentField)
    while not win:  # in determine_win_state this is changed to True if a win is detected
        if Player == 1:
            cprint("Players turn: 1", "red")  # Display the player's turn
        else:
            cprint("Players turn: 2", "green")  # Display the player's turn
        MoveColumn = int(input("Please enter the column (1, 2, 3, 4, 5, 6, 7): \n"))  # Convert the column to integer
        if 1 <= MoveColumn <= 7:
            MoveColumn -= 1  # For normal users makes more sense to start column counting at 1, /
            # but in program, use 0 as first column
            if Player == 1:
                # Make move for player 1
                # Access our current field
                # We only want to make one move when that specific field is empty
                MoveRow = determine_empty_row(MoveColumn)
                if MoveRow >= 0:
                    currentField[MoveColumn][MoveRow] = red_stone
                    # Once Player 1 make his move we change the Player to 2
                    Player = 2
            else:  # Player 2
                MoveRow = determine_empty_row(MoveColumn)
                if MoveRow >= 0:
                    currentField[MoveColumn][MoveRow] = green_stone
                    # Once Player 1 make his move we change the Player to 2
                    Player = 1

            # At the end, draw the current field representation
            draw_board(currentField)
            win = determine_win_state(currentField)
        else:
            print("invalid input")
