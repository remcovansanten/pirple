"""
Little experiments with File I/O to get aquinted with it

"""
from structshape import structshape
CountriesFile = open("countries/countries", "r")
TheWholeFile = CountriesFile.read()
#print(TheWholeFile)
CountriesFile.close()


countries_list = []
with open("countries/countries","r") as countryFile:  # With open automatically closes the file after indent
    for line in countryFile:
        line = line.rstrip("\n")
        if line == 'Netherlands':
            print('My home country is: ',end='')
            print(line)

        countries_list.append(line)

print()
print(countries_list)
print(structshape(countries_list))
countries_set = set(countries_list)
print(countries_set)  # No duplicates in set
print(structshape(countries_set))  # Structshape from import