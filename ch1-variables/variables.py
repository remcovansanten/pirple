"""
This is homework for Chapter 1, Variables
Author: Remco van Santen, remcovansanten@me.com

Details:
What's your favorite song?

Think of all the attributes that you could use to describe that song. \
    That is: all of it's details or "meta-data".
These are attributes like "Artist", "Year Released", "Genre", "Duration", etc.
Think of as many different characteristics as you can.
In your text editor, create an empty file and name it main.py
Now, within that file, list all of the attributes of the song, \
    one after another,
by creating variables for each attribute, and giving each variable a value. \
    Here's an example:
Give each variable its own line. Then, after you have listed the variables, \
    print each one of them out.
"""

title = 'What a wonderful world'   # This is the title of the song
artist = 'Eva Cassidy'              # This is the performing artist
album = 'Live at Blues Alley'
trackNbr = 12
trackLength = '5:34'                # The track length in minutes:seconds
rating = 5.0

print(title.title())
print(artist.title)
print(album.capitalize())
print(trackNbr)
print(trackLength)
print(rating)
