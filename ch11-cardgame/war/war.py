"""
Cardgame War
(c) 2021, Remco van Santen
"""


import os
from cardclass import *


# Function to clear the terminal
def clear():
    os.system("clear")

class CardGame:
    def __init__(self):
        self.deck = Deck()
        self.deck.shuffle()
        self.Stack = {}

class WarGame(CardGame):
    """
    The rules are really simple
    """

    def rules(self):
        """
        War is a card game made for a minimum of 2 players.
        The more people you play the game, the more sets of cards you need otherwise the game will end too quickly.
        The more sets you use, the longer the game will last.
        Handy with this game is that you know the order of the cards well
        2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace, Joker.

        Gameplay:
        - All cards are shuffled and distributed among the players.
          The player next to the dealer gets to start the game.
        - Everyone has their cards in a face-down pile on the table in front of them.
          In turn, each player must turn over one card from the top card of his stack.
          Important: The number of cards has to be called before turning over the cards.
        - The player who has the highest card may take any turned-over cards and place them
           in a face-down pile next to him.
          These card may be used as playing cards once his first stack is finished
        Game end:
        When a player has no cards left he is out of the game. Last player with cards wins.

        Examples
        - When a 2, 6, Jack and Ace are imposed, the player who imposed the Ace has won this round.
        - When your pile is finished, you have to shuffle the cards you have won and put them in a face down pile again,
          so you continue to play with them.
        - When a player runs out of all his cards, he is dead and out of play.
          The last person remaining is the survivor and has won the game.
        - The joker is always the highest card in the deck.
          But you can also agree in advance that the 8 beats the Joker, this makes it even more exciting.
        """

    def play(self, names):
        endofgame = False
        self.hands = []
        self.piles = []
        self.stacks = []
        for name in names:
            self.hands.append(Hand(name))
        self.deck.deal(self.hands)
        # print("---------- Cards have been dealt ----------")
        while not endofgame:
            # remove losing players
            for i in range(len(self.hands) - 1, -1, -1):
                if not self.hands[i].cards and not self.hands[i].piles:
                    print("Removed " + self.hands[i].name + " from the game.")
                    self.hands.__delitem__(i)
            for i in range(len(self.hands)):
                if len(self.hands) == 1:
                    endofgame = True
                    break
                else:
                    self.PlayRound(self.hands[i])
        print('---------------------------------------------------------')
        print('Winner of this game is: ' + self.hands[0].name)
        print('---------------------------------------------------------')

    def PlayRound(self, Player):
        if not Player.cards and not Player.piles:
            # Skip player
            assert True
            # print("Skipped player: " + Player.name)
        else:
            print('---------------------------------------------------------')
            print("It's " + str(Player.name) + "'s turn")
            print("This player has " + str(len(Player.cards)) + " cards in hand and " + str(
                len(Player.piles)) + " on the pile")

            print("Type --help for rules of the game")
            try:
                nbrofcards = input('How many cards do you want to play? (1,2 or 3)')
            except ValueError:
                print("Please try again")
                return self.PlayRound(Player)
            if nbrofcards == '--help':
                help(self.rules)
                return self.PlayRound(Player)
            elif nbrofcards == "p":
                self.printHands()
                return self.PlayRound(Player)
            elif nbrofcards == "1" \
                    or nbrofcards == "2" \
                    or nbrofcards == "3":
                self.playcards(nbrofcards)
            else:
                print("Please try again")
                return self.PlayRound(Player)

    def playcards(self, nbrofcards):
        stack = []
        self.losingPlayers = []
        self.highcard = Card()
        self.highcard.rank = 0
        self.playingcard = Card()
        self.roundwinner = Hand()
        for i in range(len(self.hands)):
            player = self.hands[i]
            for j in range(int(nbrofcards)):
                if len(player.cards) > 0:  # Player has cards in his hand
                    # self.playcard(player)
                    self.playingcard = player.cards.pop()
                    print(player.name + ' plays: ' + str(self.playingcard))
                    stack.append(self.playingcard)
                    if self.playingcard.__cmp__(self.highcard) == 1:
                        self.highcard = self.playingcard
                        self.roundwinner = player
                elif len(player.piles) > 0:  # Players has cards on his pile, so move these to the hand
                    for k in range(len(player.piles)):
                        player.addToHand(player.piles.pop())
                    # self.playcard()
                    self.playingcard = player.cards.pop()
                    print(player.name + ' plays: ' + str(self.playingcard))
                    stack.append(self.playingcard)
                    if self.playingcard.__cmp__(self.highcard) == 1:
                        self.highcard = self.playingcard
                        self.roundwinner = player
                else:  # Player has no cards
                    # print('Player ' + player.name + ' has no cards left and is out of the game')
                    self.losingPlayers.append(i)
        for i in range(len(stack)):
            self.roundwinner.addToPile(stack.pop())
        print("Added " + str(i + 1) + " to pile of roundwinner " + self.roundwinner.name)

    def printHands(self):
        for hand in self.hands:
            print(hand.name + " has the following cards in hand:")
            print(hand)

    @property
    def getPlayers(self):
        i = 1
        players = []
        pname = "dummy"
        while pname != "":
            pname = input("Name of player " + str(i) + " (Enter to quit): ")
            if pname != "":
                players.append(pname)
                i = + 1
        return players


if __name__ == '__main__':
    game = WarGame()
    players = game.getPlayers
    game.play(players)
    # game.play(["Remco", "Willeke", "Aaron", "Cerès"])
