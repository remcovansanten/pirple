"""
Classes for a card game
"""
import random


class Card:
    """
    The Card class defines the suits, cards, symbols and order of cards in a deck of cards.
    It also has methods to init, compare and print a card.
    """
    Suits = ["Spades", "Hearts", "Clubs", "Diamonds"]  # The type of suit
    suits_symbols = {"Spades": "\u2664", "Hearts": "\u2661", "Clubs": "\u2667", "Diamonds": "\u2662"}  # The suit values
    Ranks = ("2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A", "Joker")  # The types of card
    cards_values = {"2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9, "10": 10, "J": 11,
                    "Q": 12, "K": 13, "A": 14, "Joker": 15}

    def __init__(self, suit=0, rank=0):
        self.suit = suit
        self.rank = rank

    def __cmp__(self, other):
        # check the ranks
        if self.rank > other.rank: return 1
        if self.rank < other.rank: return -1
        # ranks are the same, check the suits
        if self.suit > other.suit: return 1
        if self.suit < other.suit: return -1
        # suits are also the same... it's a tie
        return 0

    def __str__(self):
        return f'{Card.Ranks[self.rank]} of {self.suits_symbols[Card.Suits[self.suit]]} {(Card.Suits[self.suit])}'


class Deck:
    def __init__(self):
        self.cards = []
        self.piles = []
        for suit in range(4):
            for rank in range(1, 14):
                self.cards.append(Card(suit, rank))

    def __str__(self):
        s = ""
        for i in range(len(self.cards)):
            s += " " * i + str(self.cards[i]) + "\n"

        if len(self.piles) > 0:
            s = s + "And has the following cards on the pile:" + "\n"
            for i in range(len(self.piles)):
                s += " " * i + str(self.piles[i]) + "\n"
        else:
            s = s + "And has no cards on the pile" + "\n"

    def print_deck(self):
        for card in self.cards:
            print(card)

    def shuffle(self):
        num_cards = len(self.cards)
        for i in range(num_cards):
            j = random.randrange(i, num_cards)
            self.cards[i], self.cards[j] = self.cards[j], self.cards[i]

    def remove(self, card):
        if card in self.cards:
            self.cards.remove(card)
            return True
        else:
            return False

    def deal_card(self):
        return self.cards.pop()

    def is_empty(self):
        return (len(self.cards) == 0)

    def deal(self, hands, num_cards=999):
        num_hands = len(hands)
        for i in range(num_cards):
            if self.is_empty(): break  # break if out of cards
            card = self.deal_card()  # take the top card
            hand = hands[i % num_hands]  # whose turn is next?
            hand.addToHand(card)  # add the card to the hand
        for i in range(num_hands):
            pile = hands[i % num_hands]
            stack = hands[i % num_hands]


class Hand(Deck):
    def __init__(self, name=""):
        self.cards = []
        self.piles = []
        self.name = name

    def __str__(self):
        s = "Hand " + self.name
        if self.is_empty():
            s = s + " is empty\n"
        else:
            s = s + " contains\n"
        return s + Deck.__str__(self)

    def addToHand(self, card):
        self.cards.append(card)

    def addToPile(self, card):  # moves cards  to players pile
        self.piles.append(card)
