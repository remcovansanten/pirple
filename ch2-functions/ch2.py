"""
Assignment #2: Functions
Author: Remco van Santen

Details:
Let's return to the music example from assignment one.
Pick 3 of the attributes you listed. For our example we're going to say "Genre", "Artist" and "Year".
Create a new Python file and create 3 functions with the same name as those attributes.
So in this example we'd have one function named "genre" another named "artist" and another called "year".

When someone calls any of those functions, that function should return the corresponding value for that attribute.
For example, if we call the "Artist" function our function would return "Dave Brubeck".
Yours should return whatever values make sense for your music choice.
"""

def title():
    return 'What a wonderful world'   # This is the title of the song

def artist():
    return 'Eva Cassidy'              # This is the performing artist

def album():
    return 'Live at Blues Alley'

def returnFalse():
    return False

def returnTrue():
    return True

t = title()
print(t)

a = artist()
print(a)
print(album())


IsTrue = returnTrue()
print ('True is: ', IsTrue)
IsFalse = returnFalse()
print('False is: ', IsFalse)
