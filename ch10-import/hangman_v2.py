"""
Project #2: Hangman

Details:
Have you ever played hangman? It's a children's game, normally played by kids when they're
supposed to be doing homework instead. If you've never played here are the rules:
https://www.youtube.com/watch?v=cGOeiQfjYPk
https://www.wikihow.com/Play-Hangman

For this assignment, we want to play hangman in 2-player mode.
The game should start by prompting player 1 to pick a word.
Then the screen should clear itself so that player 2 can't see the word
hint: print(chr(27) + "[2J")

After the screen is clear, the "gallows" and the empty letter spaces should be drawn,
and player 2 should be allowed to guess letters until they either win, or lose.
As they choose correct letters, the letters should appear on the screen in place of the blank space
(clear and redraw the whole screen). As they choose wrong letters,
the "man" himself should come end up being drawn, piece by piece.
How many guesses they get before losing is up to you (depending on how complicated of a man you want to draw).
"""
from english_words import english_words_lower_alpha_set as words
from termcolor import cprint
from random import randint
import os

clear_screen = os.system('cls' if os.name == 'nt' else 'clear')


class HangMan:

    def draw_hangman(hm_drawing):
        clear_screen
        os.system('clear')
        for row in range(7):
            for column in range(10):
                print(hm_drawing[row][column], end='')
            print('')

    def printGuessWord(guess_word, correct_letters):
        failed = 0
        for char in guess_word:
            if char in correct_letters:
                print(' ' + char + ' ', end='')
            else:
                print(' _ ', end='')
                failed += 1
        print('')
        if failed == 0:
            end_game('win', guess_word)

    def missed(nr):
        missed_row = MISSED_GUESS[nr][0]
        missed_column = MISSED_GUESS[nr][1]
        missed_symbol = MISSED_GUESS[nr][2]
        hm_drawing[missed_row][missed_column] = missed_symbol


def end_game(state, guess_word):
    if state == 'win':
        cprint('You have won', 'green')
    else:
        HangMan.draw_hangman(hm_drawing)
        cprint('You have lost', 'red')
        print('Word to guess was:', guess_word)
    exit(0)


def main():
    no_misses = 0
    correct_letters = ''
    missed_letters = []
    guess_word = list(words)[randint(0, 1000)]
    guess_list = list(guess_word)
    while True:
        HangMan.draw_hangman(hm_drawing)
        HangMan.printGuessWord(guess_word, correct_letters)
        guess_letter = input('Which letter do you want to guess?: ')
        if guess_letter in guess_list:
            correct_letters += guess_letter
        else:
            if guess_letter not in missed_letters:
                HangMan.missed(no_misses)
                no_misses += 1
                missed_letters.append(guess_letter)
                if no_misses >= 7:
                    end_game('lost', guess_word)
            else:
                print('You already guessed these letters', missed_letters)
                cprint('Letter already guessed, try again please', 'red')


if __name__ == '__main__':
    print('-----------')
    print('  HANGMAN')
    print('-----------')
    INITIAL_DRAWING = [[' ', '+', '-', '-', '-', '-', '-', '+', ' ', ' '],
                       [' ', '|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
                       [' ', '|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
                       [' ', '|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
                       [' ', '|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
                       [' ', '|', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '],
                       ['=', '=', '=', '=', '=', '=', '=', '=', '=', '=']]

    MISSED_GUESS = [[2, 7, 'O'], [3, 7, '|'], [3, 6, '/'], [3, 8, '\\'],
                    [4, 6, '/'], [4, 8, '\\'], [1, 7, '|']]
    hm_drawing = INITIAL_DRAWING
    main()
