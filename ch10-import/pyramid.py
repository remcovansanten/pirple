"""
Pyramid
It solves the puzzle where a pyramid needs to be filled with numbers
| A | B | C | D | E |
  | F | G | H | I |
    | J | K | L |
      | M | N |
        | O |
Rules:
All integers 1 - 15 included must be used once
A - B = abs(F)
B - C = abs(G)
F - G = abs(J)
etc.

It demonstrates the use of itertools.
The module standardizes a core set of fast, memory efficient tools that are useful by themselves or in combination. \
Together, they form an “iterator algebra” making it possible to construct specialized tools succinctly and efficiently \
in pure Python.

Copyright: (2020) Remco van Santen
"""
import itertools
import time


def pyramid():
    counter = 0
    logic_counter = 0
    loopTime = time.time()
    for b in itertools.combinations(range(1, max_num + 1),
                                    pyr_width):  # fetches all possible combinations from the possible numbers
        for xs in itertools.permutations(b):  # For evey found combination, hustles the numbers in every position
            rows = [xs]
            counter += 1
            if counter % 10000000 == 0:
                print('Counter:', counter, ' Logical Counter:', logic_counter, 'Elapsed time: ', time.time() - loopTime)
                loopTime = time.time()
            if max_num in xs:  # Logic: max_num needs to be in top row. difference can never be
                logic_counter += 1
                while len(rows[-1]) != 1:
                    rows.append(tuple(abs(a - b) for a, b in zip(rows[-1], rows[-1][1:])))
                used = sum(rows, ())
                if all(1 <= i <= max_num for i in used) and len(set(used)) == max_num:
                    yield rows


if __name__ == '__main__':  # Main loop
    default = 5
    try:
        pyramid_height = int(input("height of the pyramid (default %s): " % int(default) + chr(8)))
        print('pyramid height chosen is: ', int(pyramid_height))
    except Exception as e:
        print('Default value: ', default, 'is chosen.')
        pyramid_height = default
        pass
    max_num = 0
    pyr_width = 0
    for i in range(1, pyramid_height + 1):  # dynamically
        max_num = max_num + i
        pyr_width = pyr_width + 1
    startTime = time.time()
    for p in pyramid():
        for row in p[::1]:
            print(' ' * 2 * (pyr_width - len(row)) + '  '.join('% 2d' % n for n in row))
    print("Total processing time", time.time() - startTime)
