"""
Details:

Pick any library that come with Python (https://docs.python.org/3/library/) \
that we haven't covered in the course already.

Learn how to use the library extensively, then prepare a code sample that showcases what you've learned.
This can take any form you wish. You could create an application with the library,
or just show examples of how to use its methods.


In this program I am going to use the itertools.
-> The module itertools standardizes a core set of fast, memory efficient tools that are useful by
themselves or in combination.
Together, they form an “iterator algebra” making it possible to construct specialized tools
succinctly and efficiently in pure Python.

"""
import itertools


class iTools:

    def gen_iter_list(start, step, max):
        """
        Generates a list as return value with iterated values
        :param start: The starting value of the returned list
        :param step: the step value
        :param max: the max value (included) of the returned list
        :return: list with iterated values
        """
        result = []
        for it in itertools.count(start, step):
            if it > max:
                break
            result.append(it)
        return result


if __name__ == '__main__':
    # tests the gen_iter_list with integers
    ret_int = iTools.gen_iter_list(2, 6, 100)
    print('gen_iter_list: int:', ret_int)
    # tests the gen_iter_list with floats
    ret_float = iTools.gen_iter_list(0.5, 4.36, 80)
    print('gen_iter_list: float:', ret_float)

    # isslice is used to take a slice of a list
    # islice(iterable, start, stop)
    # ireturn s the start element (starting from 0) up to, but not including the stop nth element
    # in this exaplme, the ret_int from the previous example is taken as iterable
    print('slice: (5,9): ', end='')
    for slice in itertools.islice(ret_int, 5, 9):
        print(slice, ' ', end='')
    print('')

    # itertools.permutations
    # https://docs.python.org/3/library/itertools.html#itertools.permutations
    # permutations return all possible combinations without duplicates
    # If r is not specified or is None, \
    # then r defaults to the length of the iterable and all possible full-length permutations are generated
    iterable = 'ABCD'
    ret = []
    print("Permutations: ", end="")
    for i in itertools.permutations(iterable, 2):
        ret.append(i)
    print(ret)

    # itertools.product
    # https://docs.python.org/3/library/itertools.html#itertools.product
    # Roughly equivalent to nested for-loops in a generator expression. \
    # For example, product(A, B) returns the same as ((x,y) for x in A for y in B).

    # example product 1
    ret = []
    iterable1 = [1, 2, 3]
    iterable2 = [8, 9]
    print("product1: ", end="")
    for i in itertools.product(iterable1, iterable2):
        ret.append(i)
    print(ret)

    # example product 2 (with repeat = 2)
    ret = []
    iterable1 = ['A', 'B']
    iterable2 = [7, 8]
    print("product2: ", end="")
    for i in itertools.product(iterable1, iterable2, repeat=2):
        ret.append(i)
    print(ret)

    # example repeat
    # Make an iterator that returns object over and over again.
    # Runs indefinitely unless the times argument is specified.
    ret = []
    iterable = 'A'
    times = 5
    print("repeat: ", end="")
    for i in itertools.repeat(iterable, times):
        ret.append(i)
    print(ret)

    # Zip Longest
    # itertools.zip_longest(*iterables, fillvalue=None)
    ret = []
    iterable1 = 'ABCDEF'
    iterable2 = '123'
    print("zip longest: ", end="")
    for i in itertools.zip_longest(iterable1, iterable2, fillvalue='-'):
        ret.append(i)
    print(ret)
