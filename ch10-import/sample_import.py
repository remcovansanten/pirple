"""
just some test code made during lectures

"""
import random
import time
import math

for i in range(1, random.randint(1, 10)):
    start = time.perf_counter()
    print('Hello World took ', end='')
    end = time.perf_counter()
    print(end - start, 'seconds to print')

val = 3.141592653589793
valFloor = math.floor(val)
valCeil = math.ceil(val)

print(math.factorial(valFloor))
print(math.sqrt(valFloor))
print(math.sin(val))
print(math.pi)

rand = random.uniform(100, 1000)
print(rand)

factVal = math.factorial(5)
temp = math.exp(5)
print('temp', temp)
