"""
Homework Assignment #9: Classes
Author: Remco van Santen

Details:
Create a class called "Vehicle" and methods that allow you to set the "Make", "Model", "Year,", and "Weight".
The class should also contain a "NeedsMaintenance" boolean that defaults to False,
and and "TripsSinceMaintenance" Integer that defaults to 0.

Next an inheritance classes from Vehicle called "Cars".
The Cars class should contain a method called "Drive" that sets the state of a boolean isDriving to True.
 It should have another method called "Stop" that sets the value of isDriving to false.

Switching isDriving from true to false should increment the "TripsSinceMaintenance" counter.
And when TripsSinceMaintenance exceeds 100, then the NeedsMaintenance boolean should be set to true.

Add a "Repair" method to either class that resets the TripsSinceMaintenance to zero, and NeedsMaintenance to false.

Create 3 different cars, using your Cars class, and drive them all a different number of times.
 Then print out their values for Make, Model, Year, Weight, NeedsMaintenance, and TripsSinceMaintenance

Extra Credit:
Create a Planes class that is also an inheritance class from Vehicle.
Add methods to the Planes class for Flying and Landing (similar to Driving and Stopping), but different in one respect:
Once the NeedsMaintenance boolean gets set to true, any attempt at flight should be rejected (return false),
and an error message should be printed saying that the plane can't fly until it's repaired.
"""
from random import randint


class Vehicle:
    """
    Vehicle class, meta class for all vehicles
    Create a class called "Vehicle".
    The class should also contain a "NeedsMaintenance" boolean that defaults to False,
    TripsSinceMaintenance" Integer that defaults to 0.
    """

    def __init__(self, make, model, year, weight, needs_maintenance=False, trips_since_maintenance=0):
        self.Make = make
        self.Model = model
        self.Year = year
        self.Weight = weight
        self.NeedsMaintenance = needs_maintenance
        self.TripsSinceMaintenance = trips_since_maintenance

    def __str__(self):
        return ' This ' + self.Make + ' ' + self.Model + ' from year: ' + str(self.Year) + ' weighs: ' \
               + str(self.Weight) + 'kg. and made: ' + str(self.TripsSinceMaintenance) + \
               ' trips since last maintenance \n thus maintenance status is: ' + str(self.NeedsMaintenance) + '\n'

    # setters
    # methods that allow you to set the "Make", "Model", "Year,", and "Weight"
    def set_make(self, m):
        """
        sets make (producer) of the vehicle
        :param m: the new make value
        :return: nothing
        """
        self.Make = m

    def set_model(self, o):
        """
        set model of vehicle
        :param o: new model value
        :return:
        """
        self.Model = o

    def set_year(self, y):
        """
        Sets year vehicle was build
        :param y: year vehicle was build
        :return: none
        """
        self.Year = y

    def set_weight(self, w):
        """
        Sets the weight of the vehicle
        :param w: weight
        :return: none
        """
        self.Weight = w

    def repair(self):
        """
        Repair method to either class that resets the TripsSinceMaintenance to zero, \
        and NeedsMaintenance to false.
        :return: string 'Car repaired'
        """
        self.TripsSinceMaintenance = 0
        self.NeedsMaintenance = False
        print(self.Make, self.Model, ' repaired')


class Cars(Vehicle):
    """
    Cars class, subclass of Vehicles
    Done: Next an inheritance classes from Vehicle called "Cars".
        The Cars class should contain a method called "Drive" that sets the state of a boolean isDriving to True.
        It should have another method called "Stop" that sets the value of isDriving to false.
    """

    def __init__(self, make, model, year, weight, needs_maintenance=False, trips_since_maintenance=0, is_driving=False):
        Vehicle.__init__(self, make, model, year, weight, needs_maintenance, trips_since_maintenance)
        self.NeedsMaintenance = needs_maintenance
        self.TripsSinceMaintenance = trips_since_maintenance
        self.isDriving = is_driving

    # Done: Switching isDriving from true to false should increment the "TripsSinceMaintenance" counter.
    #    And when TripsSinceMaintenance exceeds 100, then the NeedsMaintenance boolean should be set to true.
    def drive(self):
        """
        Set isDriving of Cars class to True
        :return:
        """
        self.isDriving = True

    def stop(self):
        """
        Set isDriving of Cars class to False
        :return:
        """
        self.isDriving = False
        self.TripsSinceMaintenance += 1
        if self.TripsSinceMaintenance > 100:
            self.NeedsMaintenance = True


class Planes(Vehicle):
    """
    Plane class, subclass of Vehicles
    Done: Create a Planes class that is also an inheritance class from Vehicle.
        Add methods to the Planes class for Flying and Landing (similar to Driving and Stopping), \
        but different in one respect:
        Once the NeedsMaintenance boolean gets set to true, any attempt at flight should be rejected (return false),
        and an error message should be printed saying that the plane can't fly until it's repaired.
    """

    def __init__(self, make, model, year, weight, needs_maintenance=False, trips_since_maintenance=0, is_flying=False):
        Vehicle.__init__(self, make, model, year, weight, needs_maintenance, trips_since_maintenance)
        self.Make = make
        self.Model = model
        self.Year = year
        self.Weight = weight
        self.NeedsMaintenance = needs_maintenance
        self.TripsSinceMaintenance = trips_since_maintenance
        self.IsFlying = is_flying

    def flying(self):
        """
        Set the plane to flying if not already flying
        checks if needs maintenance is false, otherwise returns false and flying is not set
        :return: true/false
        """
        if self.NeedsMaintenance:
            print("plane can't fly until it's repaired")
            return False
        else:
            self.IsFlying = True

    def landing(self):
        """
        Sets 'IsFlying' to False and adds 1 to trips.
        If Trips > 100 set needs maintenance to yes
        :return: True / False
        """
        if self.IsFlying:
            self.IsFlying = False
            self.TripsSinceMaintenance += 1
            if self.TripsSinceMaintenance > 100:
                self.NeedsMaintenance = True
            return True
        else:
            print("Plane isn't flying")
            return False


def sub_car():
    """
    Sub process for the cars
    :return: none
    """
    # Create 3 different cars, using your Cars class, and drive them all a different number of times./
    car1 = Cars("Tesla", "Model 3", 2019, 1400, False, 0)
    car2 = Cars("Fiat", "500", 2012, 1100, False, 20)
    car3 = Cars("Fiat", "500", 1968, 650, False, 40)

    for i in range(440):
        x = randint(1, 3)
        if x == 1:
            if car1.isDriving:
                car1.stop()
            else:
                car1.drive()
        elif x == 2:
            if car2.isDriving:
                car2.stop()
            else:
                car2.drive()
        elif x == 3:
            if car3.isDriving:
                car3.stop()
            else:
                car3.drive()
    # Done: Then print out their values for Make, Model, Year, Weight, NeedsMaintenance, and TripsSinceMaintenance
    print(car1)
    print(car2)
    print(car3)
    if car1.NeedsMaintenance:
        car1.repair()
    if car2.NeedsMaintenance:
        car2.repair()
    if car3.NeedsMaintenance:
        car3.repair()
    print("____ Cars repaired _____")
    print(car1)
    print(car2)
    print(car3)


def sub_plane():
    """
    Sub process for the planes
    :return: None
    """
    plane1 = Planes("Boeing", "747", 1988, 154090, False, 0)
    plane2 = Planes("Boeing", "737", 2000, 111000, False, 0)
    plane3 = Planes("Airbus", "380-WB", 2012, 144000, False, 00)

    for i in range(840):
        x = randint(1, 3)
        if x == 1:
            if plane1.IsFlying:
                plane1.landing()
            else:
                ret = plane1.flying()
                if ret is False and plane1.NeedsMaintenance:
                    print("Plane 1 needs maintenance, so will be repaired")
                    plane1.repair()
        elif x == 2:
            if plane2.IsFlying:
                plane2.landing()
            else:
                ret = plane2.flying()
                if ret is False and plane2.NeedsMaintenance:
                    print("Plane 2 needs maintenance, so will be repaired")
                    plane2.repair()
                else:
                    plane2.flying()
        elif x == 3:
            if plane3.IsFlying:
                plane3.landing()
            else:
                ret = plane3.flying()
                if ret is False and plane3.NeedsMaintenance:
                    print("Plane 3 needs maintenance, so will be repaired")
                    plane3.repair()
                else:
                    plane3.flying()
    # Done: Then print out their values for Make, Model, Year, Weight, NeedsMaintenance, and TripsSinceMaintenance
    print("--------- End of Flights. Current status: -------------")
    print(plane1)
    print(plane2)
    print(plane3)


if __name__ == "__main__":
    sub_car()
    sub_plane()
