"""
Extra Credit:

Create a function that allows someone to guess the value of any key in the dictionary,
and find out if they were right or wrong. This function should accept two parameters: Key and Value.
If the key exists in the dictionary and that value is the correct value, then the function should return true.
In all other cases, it should return false.
"""
ValueDict = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f': 6}


def checkguess(key, value):
    if key in ValueDict:
        if str(ValueDict[key]) == str(value):
            return True
        else:
            return False
    else:
        return False


# main loop
keyVal = input('Please enter the key value:    ')
ValVal = input('Please enter the Search value: ')

print(checkguess(keyVal, ValVal))
