
"""
Homework Assignment #7: Dictionaries and Sets

Details:
Return to your first homework assignments, when you described your favorite song.
Refactor that code so all the variables are held as dictionary keys and value.
Then refactor your print statements so that it's a single loop that passes through each item in the dictionary ' \
                                              'and prints out it's key and then it's value.

title = 'What a wonderful world'   # This is the title of the song
artist = 'Eva Cassidy'              # This is the performing artist
album = 'Live at Blues Alley'
trackNbr = 12
trackLength = '5:34'                # The track length in minutes:seconds
rating = 5.0
"""
favArtist = {'Title': 'What a wonderful world','artist': 'Eva Cassidy','album': 'Live at Blues Alley','trackNbr' : '12', 'trackLength' : '5:34', 'rating' : 5.0 }

for key in favArtist:
    print(key, ' = ', favArtist[key])


