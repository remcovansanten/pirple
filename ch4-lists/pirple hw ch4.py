"""
Homework Assignment #4: Lists
Author: Remco van Santen

Details:
Create a global variable called myUniqueList. It should be an empty list to start.

Next, create a function that allows you to add things to that list.
Anything that's passed to this function should get added to myUniqueList, unless its value already exists in myUniqueList. ' \
             'If the value doesn't exist already, it should be added and the function should return True.
If the value does exist, it should not be added, and the function should return False;

Finally, add some code below your function that tests it out.
It should add a few different elements, showcasing the different scenarios,
and then finally it should print the value of myUniqueList to show that it worked.

Extra Credit:

Add another function that pushes all the rejected inputs into a separate global array called myLeftovers.
If someone tries to add a value to myUniqueList but it's rejected (for non-uniqueness), ' \
                                                      'it should get added to myLeftovers instead.
"""
import pytest
myUniqueList = []


def addtomyuniquelist(input_var):
    if input_var in myUniqueList:
        return False
    else:
        myUniqueList.append(input_var)
        return True


print(addtomyuniquelist(45))
print(addtomyuniquelist(45))
print(addtomyuniquelist(45))
print(addtomyuniquelist('My name is Remco'))
print(addtomyuniquelist(['another', 'nested', 'list']))
print(addtomyuniquelist(['another', 'nested', 'list']))
print(addtomyuniquelist(89))
print(addtomyuniquelist(89.1))
print(addtomyuniquelist(89.2))
print(addtomyuniquelist(89))
print(myUniqueList)

"""
Extra Credit
"""
myUniqueList = []
myLeftovers = []


def addtomylists(input_var):    # 95% copy of  addToMyUniqueList
    if input_var in myUniqueList:
        myLeftovers.append(input_var)  # added this line to original function addToMyUniqueList
        return False
    else:
        myUniqueList.append(input_var)
        return True


print(addtomylists(45))
print(addtomylists(45))
print(addtomylists(45))
print(addtomylists('My name is Remco'))
print(addtomylists(['another', 'nested', 'list']))
print(addtomylists(['another', 'nested', 'list']))
print(addtomylists(89))
print(addtomylists(89.1))
print(addtomylists(89.2))
print(addtomylists(89))
print('myUniqueList:', myUniqueList)
print('myLeftovers: ', myLeftovers)
